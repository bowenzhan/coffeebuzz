<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Auth;
use Validator;

class StaffsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manager.users.index')
            ->with('users',
                User::whereHas('roles', function ($q) {
                    $q->whereIn('name', ['staff']);
                })->paginate(10)
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manager.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('manager.users.create')
                ->withErrors($validator)
                ->withInput();
        }

        $staff = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $staffRole = Role::where('name', 'staff')->first();
        $staff->roles()->attach($staffRole);

        return redirect()
            ->route('manager.users.index')
            ->with('success', 'A new staff created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('manager.users.edit')
            ->with(['user' => User::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('manager.users.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($id);
        if ($user) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);

            $user->save();
            return redirect()
                ->route('manager.users.index')
                ->with('success', 'Staff updated successfully!');
        }

        return redirect()
            ->route('manager.users.index')
            ->with('warning', 'This staff can not be edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->roles()->detach();
            $user->delete();
            return redirect()
                ->route('manager.users.index')
                ->with('success', 'Staff has been deleted.');
        }

        return redirect()
            ->route('manager.users.index')
            ->with('warning', 'This staff can not be deleted.');
    }
}
