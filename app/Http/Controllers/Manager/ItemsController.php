<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manager.items.index')
            ->with('items', Item::paginate(10));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('manager.items.edit')
            ->with(['item' => Item::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'price' => 'required|numeric|gte:0'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('manager.items.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $item = Item::find($id);
        if ($item) {
            $item->name = $request->name;
            $item->price = $request->price;

            $item->save();
            return redirect()
                ->route('manager.items.index')
                ->with('success', 'Item updated successfully!');
        }

        return redirect()
            ->route('manager.items.index')
            ->with('warning', 'This item can not be edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        if ($item) {
//            $item->roles()->detach();
            $item->delete();
            return redirect()
                ->route('manager.items.index')
                ->with('success', 'Item has been deleted.');
        }

        return redirect()
            ->route('manager.items.index')
            ->with('warning', 'This item can not be deleted.');
    }
}
