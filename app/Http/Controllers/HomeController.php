<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->hasAnyRole('manager')) {
            return redirect()->route('manager.users.index');
        }

        if ($user->hasAnyRole('staff')) {
            return redirect()->route('staff.order');
        }

        return view('home');
    }
}
