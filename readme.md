##CoffeeBuzz
This is a web application uses Laravel PHP framework. This project is developed for 
Assignment 1 - ISYS1106/1108 Software Engineering Project Management.

##Scenario
A coffee shop 4U is located at the University Campus. They have very long queues in the morning and during the lunch time, especially before/after lecture classes in the large lecture theatres nearby. They use currently a simple paper ticketing system, but think that it is not really time-saving and too “old-school”. They would like to automate the ordering process and to have a cashless payment only. 
For that, they would like to have 
-	Android and iOS apps for the customers to order coffee,
-	a responsive Web-interface for staff and/or for customers (should work well on an old laptops and on new tablets) to process the orders. 
 
The following details are available after the meeting with project sponsor:
They serve hot coffee in small, medium, and large cups.
Espresso and double espresso can be served only as a small cup.
Latte can be served only as a large cup.
Cappuccino, long black, hot chocolate and tee (Earl Grey, Assam, Green, and Mint) can be served as medium and large cups.
The shop also provides a range of cakes and sandwiches, the range of them changes periodically. If some of the cakes or sandwiches are sold out, the customer should be able to see this.
 
All staff members require to login using an identifier / user id. 
The system should be able to display to the batista the list of current requests. When the order is ready, the barista is indicating this in the system and the customer’s application presents on its screen the corresponding message and the order number (should be also large enough for barista to check quickly).
The manager should be able to add new staff members to the system and revoke access if a staff member is no longer working at the coffee shop. 

Customers need to know how many orders are now in the queue and how long (approx.) they might wait if they order. 

The system should accept PayPal and credit card payments.

The deliverable for this project are:

-	Web application (for staff and/or for customers) and its successful deployment on a domain, 
-	training sessions for the staff,
-	demo video for the customers, and
-	documentation created during project management. 

##Deployment
To run this project locally, you will need **PHP >= 7.13** and **MySQL** installed on your machine. Please refer to [Laravel](https://laravel.com/docs/5.8) for more details.

You will need to create a Schema in MySQL database called **coffeebuzz**, and a user with username **coffeebuzz** and password **coffeebuzz** associated with the schema. 
Please note the Authentication Type for the user should be **Standard**, otherwise Laravel won't be able to connect to the database.

After you installed the required tools and setup database, you can use the following command in project directory to create tables and initial data in database.
```
php artisan migrate
php artisan db:seed
```

Then you can use the following command to run the project locally.
```
php artisan serve
```

**The default manager account:**  
username: bowen@coffeebuzz.com  
password: manager

**The default staff account:**  
username: james@coffeebuzz.com  
password: staff

**The default customer account:**  
username: tom@gmail.com  
password: customer