<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use FakerRestaurant\Provider\en_US\Restaurant;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Item::class, function (Faker $faker) {

    $faker->addProvider(new Restaurant($faker));

    return [
        'name' => $faker->unique()->foodName(),
        'price' => $faker->randomFloat(2, 0, 50),
    ];
});
