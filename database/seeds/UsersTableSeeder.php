<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear user and user role relation table
        User::truncate();
        DB::table('role_user')->truncate();

        $managerRole = Role::where('name', 'manager')->first();
        $staffRole = Role::where('name', 'staff')->first();
        $customerRole = Role::where('name', 'customer')->first();

        // create default users
        $defaultManager = User::create([
            'name' => 'Bowen',
            'email' => 'bowen@coffeebuzz.com',
            'password' => bcrypt('manager')
        ]);

        $defaultStaff = User::create([
            'name' => 'James',
            'email' => 'james@coffeebuzz.com',
            'password' => bcrypt('staff')
        ]);

        $defaultCustomer = User::create([
            'name' => 'Tom',
            'email' => 'tom@gmail.com',
            'password' => bcrypt('customer')
        ]);

        // attach roles to default users
        $defaultManager->roles()->attach($managerRole);
        $defaultStaff->roles()->attach($staffRole);
        $defaultCustomer->roles()->attach($customerRole);

        // create dummy staffs
        factory(App\User::class, 25)->create();
    }
}
