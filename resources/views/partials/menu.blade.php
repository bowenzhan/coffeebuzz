@auth
    <ul class="navbar-nav mr-auto">
        {{-- show management dropdown if user is a manager --}}
        @if(Auth::user()->hasAnyRole('manager'))
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Management
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('manager.users.index') }}">Staffs</a>
                    <a class="dropdown-item" href="#">Sales</a>
                    <a class="dropdown-item" href="{{ route('manager.items.index') }}">Items</a>
                </div>
            </li>
        @endif
        {{-- show storage and orders if user is a staff --}}
        @if(Auth::user()->hasAnyRoles(['manager','staff']))
            <li class="nav-item active">
                <a class="nav-link" href="#">Storage</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="#">Orders</a>
            </li>
        @endif
    </ul>
@endauth