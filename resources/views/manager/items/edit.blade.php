@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage {{ $item->name }}</div>

                    <div class="card-body">
                        <form action="{{ route('manager.users.update', ['user' => $item->id]) }}" method="post">
                            @csrf
                            {{ method_field('PUT') }}
                            {{--update user details here--}}
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
