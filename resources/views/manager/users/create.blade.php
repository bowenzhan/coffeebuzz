@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create New Staff</div>

                    <div class="card-body">
                        <form action="{{ route('manager.users.store') }}" method="post">
                            @csrf
                            {{--create staff here--}}
                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
